# Gitlab Pipeline setup
---
##### This file will describe the steps to setup a simple gitlab pipeline for a gitlab repository

## Quick overview:
Gitlab has a buit-in pipeline feature and it can be setuped as follows:
* There are 3 main parts to make a pipeline in gitlab
1. The project repository.
2. The .gitlab-ci.yml file, which describe the pipeline and how it works.
3. The gitlab-runner, which is the place (server or system) where the pipeline is run.
---
## 1. The project repository:
1. From gitlab, create a new blank project and name it.
2. Click the clone button and select the HTTPS link or select the SSH link if you an SSH key (steps for generating on is below).
3. The HTTPS link will ask you for the gitlab username and password each time you make a push command to the repo.
    * To avoid typing credentials each time, SSH authentication can be used, but it requires a few steps. You can skip this section if you want, and go to step 4!
        1. Generate an SSH key on your machine (The command differs according to the OS), the steps for linux is as follows:
            * In the terminal ```cd ~/.ssh ```
            * If a file named ```id_res.pub``` exists, use it. Otherwise run ```ssh-keygen``` and press enter till the key is generated.
            * Now two files should be generated ```id_res``` whic is the private key, and ```id_res.pub``` which is the public key.
         2. Add the generated public key to your gitlab account:
            * From the same previus directory run ```cat id_res.pub``` in the terminal to print the content of the ```id_res.pub``` in the terminal.
            * Copy it and go to your gitlab account, and select (preferences) or (settings) from the menu in the upper right.
            * Select the (SSH Keys) option from the menu on the left.
            * In the text box under the (Key) section, paste your public key copied from the ```cat id_res.pub``` command
            * Add a title to your key in the next textbox
            * Add an expiration date for the key (Optional)
            * Click ```Add Key```
            * Now you should be able to make push commands on your machine (Only the machine that you generated the key from of course 😅) to github without entering you credentials each time ✨
4. Go to a directory on your machine and initialize a local git repo by running ```git init``` in the terminal.
5. Connect the local repo to the gitlab repo by running ```git remote add origin <Cloned repo URL>``` add the repo URL without brackets
6. Now you can add files and write code then push the changes to the repo using noraml git commands.
---
## 2. The .gitlab-ci.yml file:
Add this file to the project repo you want the pipeline to work on. This is simply a .yml file describing the pipeline jobs. Depending on what is needed to be done, the file is changed, so only a simple example is described here. 
A simple ```.gitlab-ci.yml``` is as follows:
```yml
stages:
  - test
  - deploy

test-job:
  stage: test  
  tags: 
    - ci-runner 
  script: 
    - echo "This is a test job"

deploy-job:
  stage: deploy
  tags:
    - ci-runner2
  script:
    - echo "This is a deploy job"
```
The file organization is as follows:
* mention the pipeline stages you want under the ```stages``` section. In this example there are two stages ```test``` and ```deploy```. At least one instance of a stage should be defined after the ```stages``` section or the stage will not be executed (No wonders!😒)
* Stages are executed in the pipeline according to the order in the ```stages``` section and not according to order for defining the stages.
    * In this example the order is ```test``` then ```deploy```.
* The next two blocks are for the stages definitions, the one name ```test-job``` is for defining a job in the ```test``` stage, and the second block which is named ```deploy-job``` is for defining a job in the ```deploy``` stage.
* Now let us understand each job:
```yml
test-job:
  stage: test  
  tags: 
    - ci-runner 
  script: 
    - echo "This is a test job"
```
* This job is named ```test-job```, you can choose any name you want.
* The line ```stage: test``` tells that this job will run in the ```test``` stage, and Yes you can define many jobs having the ```stage: test``` defined, and they will all run parallel in the ```test``` stage in the pipeline. The following images makes it clear.
![Pipeline example in Gitlab](images/pipelines.png)

    In this image the pipeline have 4 stages named (Build, Test, Staging, Production). In the ```Test``` stage there are 2 jobs running in parallel, because in the .gitlab-ci.yml file of that pipeline, both jobs named ```test1``` and ```test2``` had this line ```stage: Test``` in their definition.
* As seen in the image also, no stage can start without the previous stage has finished execution successfully. So, if the ```Test``` failed for example, the ```Staging``` and hence the ```Production``` stages will not start. This makes sure that only successful test can reach the deployment stage.
* Now back to our code example. 
```yml  
  tags: 
    - ci-runner 
```
* This mentions the runner on which this job is run. Simply, the runner can be defined on a machine you want to use to run the pipeline on, then the runner is given tags that can be used in jobs defintion in the ```.gitlab-ci.yml``` file to use that runner with that job. There is a runner section talking about the setup of a runner in details.
```yml
  script: 
    - echo "This is a test job"
```
* In the ```script``` section, you can run terminal commands to do your required job.
* That was a simple example for the ```.gitlab-ci.yml``` file.
* When changes are made in a local repo that contains a ```.gitlab-ci.yml``` file, then the changes are pushed to gitlab repo. The pipeline defined in the ```.gitlab-ci.yml``` file will start automatically.
* For more check this link: https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html
---

## 3. The gitlab-runner:
The offical docs:  https://docs.gitlab.com/runner/
A runner is an open source continuous integration service included with GitLab. It is used to run jobs & send results back to GitLab. Gitlab runner must be installed on any system where you want to define a runner. Many runners can be defined on the same system, but each one should have a unique tag so that it can be targeted in the previuosly mentioned ```.gitlab-ci.yml``` file.
Gitlab offers shared-runners that use their infrastructure to run pipelines on, so there is an option to run pipelines without making your own runners. However, it is not always free of cost, and there may be privacy concerns to run your pipeline on a shared-runner.
So it is better to turn-off shared runners in your project and make your own runner if this option is available.

##### The section is divided into 3 parts:
1. How to install GitLab runner
2. How to register GitLab runner
3. How to start GitLab runner

#### Step 1: Install GitLab Runner
* You will need to install gitlab-runner on your system once, then you can define as many runners as much as you want to run on your system.
* Steps to install gitlab-runner depending on your system is found in this link:  https://docs.gitlab.com/runner/install/index.html

* Check that gitlab-runner is installed by running this command in the terminal or cmd:
    ```gitlab-runner --version```
    If it retuened that gitlab-runner is not a defined command, make sure to add the installation path to the system environment variables, or run the command from the installation directory.
   
#### Step 2: Register GitLab Runner

* To define a runner for a certain project on gitlab depending on your system check this documentation:
    https://docs.gitlab.com/runner/register/index.html
* For linux system:
    1. run ```sudo /usr/local/bin/gitlab-runner register```
    2. Enter your GitLab instance URL (also known as the gitlab-ci coordinator URL)
        * For a normal gitlab account it will be ```https://gitlab.com/```, but for Corporatica gitlab it will be ```https://gitlab.corporatica.com/```. You can find it by entering a project repo on gitlab, then from the left menu choose ```setting -> CI/CD```, then at the ```Runner``` section click ```Expand```, scroll down till the ```Set up a specific runner manually``` title and you will find the ```GitLab instance URL```.
     3. Enter the token you obtained to register the runner.
        * It is found under ```GitLab instance URL``` mentioned in previous step.
     4. Enter a description for the runner. You can change this value later in the GitLab user interface.
     5. Enter the tags associated with the runner, separated by commas. You can change this value later in the GitLab user interface. These are the tags used to target a runner in the ```.gitlab-ci.yml``` file.
     6. Provide the runner executor. Till now my used case is ```shell```, but on the official documentation it says the most use case is ```docker```. I did not require it till now, but I think I will.
     7. If you entered ```docker``` as your executor, you’ll be asked for the default image to be used for projects that do not define one in ```.gitlab-ci.yml```.
     * That's is, now a gitlab-runner is defined for your gitlab project. But there is still only one step.😌
     * By default, a gitlab-runner is open only for the gitlab project it is defined with, but this can be changed to be open for all of your projects on gitlab.

#### Step 3: Start GitLab Runner
* To start defined gitlab-runners, run the following command in terminal:
    ```sudo /usr/local/bin/gitlab-runner start```
    It can be run without ```sudo``` but it needs ```sudo``` if you want the runner to start automatically.
* Now you can go to ```runners``` section and click ```expand``` as described in the previous steps, and scroll down to see your defined gitlab runners. If they are running correctly, a green circle should appear on their left.
* To stop a runner run this command in the terminal:
    ```sudo /usr/local/bin/gitlab-runner stop```
    or you can stop it from gitlab under the ```runners``` section in your project.
##### For fixing "new runner, has not connected yet" warning
* Run the following command in the terminal:
    ```sudo /usr/local/bin/gitlab-runner verify```

#### Step 4: Use the runner!
Now you can use your gitlab-runner in a pipeline by using the runner's tag in the ```.gitlab-ci.yml``` file as mentioned in the section of ```.gitlab-ci.yml``` file.

--------------------------------------------------------------
#### Gitlab-runner tips:

* If job says ```job is pending waiting for the runner use``` and there is no other job working, use this command:	
	```sudo /usr/local/bin/gitlab-runner run```

* You can find the ```config.toml``` file in:
    ```/etc/gitlab-runner/``` on *nix systems when GitLab Runner is executed as root (this is also the path for service configuration). This file is used to edit the defined runners on this machine.
    * if permission is denied use: 
    ```sudo su```
    ```cd /etc/gitlab-runner/```
    Edit the ```config.toml``` file, then to exit super-user mode type: ```exit```
								
* You can use a ```shell``` executer runner and use docker engine installed on the server on which the runner is defined (can be your machine, EC2 instance, ...etc).
	Note: The runner now have access to the docker containers and images on the server and can delete or manage them so take care will making the ```.gitlab-ci.yml``` file. In this case, you are basically running bash commands on the server (The host of the runner) but through gitlab.
* For more ways to used docker with the gitlab-runner, check this link:
	https://www.balticlsc.eu/gitlab/help/ci/docker/using_docker_build.md

Enjoy 😄



